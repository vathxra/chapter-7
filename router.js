const express = require ("express");
const authController = require ("./controllers/authController");
const roomController = require ("./controllers/roomController");
const authorize = require ("./middlewares/authorize");
const role = require("./middlewares/isAdmin");

const router = express.Router();

router.get("/api/auth/me", authorize, authController.whoami);
router.get("/api/auth/admin-only", authorize, role(['admin']), authController.isAdmin)

router.post("/login", authController.login);
router.post("/api/auth/register", authController.register);

router.post("/create-room", authorize, roomController.createRoom);
router.put("/fight-room/p1/:id", authorize, role(['user']), roomController.p1value);
router.put("/fight-room/p2/:id", authorize, role(['user']), roomController.p2value);
router.post("/room/result/:id", authorize, role(['user']), roomController.result);
router.get("/game-history/:id", authorize, role(['admin']), roomController.gameHistory);

module.exports = router;