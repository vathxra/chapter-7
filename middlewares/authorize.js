const jwt = require ("jsonwebtoken");
const {JWT_SIGNATURE_KEY = "FASYA"} = process.env

module.exports = function (req, res ,next) {
    try {
        const payload = jwt.verify(req.headers["authorization"], JWT_SIGNATURE_KEY);
        req.user = payload;
        next();
    } catch (err) {
        res.status(401).json({
            status : "FAIL",
            data : {
                name : "UNAUTHORIZED",
                message : err.message
            }
        });
    }
}