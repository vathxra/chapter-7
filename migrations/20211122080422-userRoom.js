'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userRooms', {
	    userId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
		        model: {
			          tableName: "Users",
		        },
		      key: "id",
		    },
        allowNull: false,
        primaryKey: true,
	    },
	    roomId: {
		    type: Sequelize.DataTypes.INTEGER,
		    references: {
		        model: {
			        tableName: "Rooms",
		        },
		        key: "id",
		    },
        allowNull: false,
        primaryKey: true,
	    },
	    createdAt: {
        allowNull: false,
        type: Sequelize.DATE
	    },
	    updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
	    }
	});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userRooms');
  }
};
