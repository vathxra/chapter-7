const { Room, userRoom } = require ("../models");

exports.createRoom = async function (req,res){
    try {  
        const user = await Room.create({
            nameRoom : req.body.room
        });

        return res.status(201).json({
            status :"OK",
            data : {
                id : user.id,
                room : user.nameRoom,
                message : "Your Room Has Been Created"
            }
        });
    } catch (err) {
        return res.status(401).json({
            status : "FAIL",
            data : {
                name : "UNPROCESSABLE DATA",
                message : err.message
            }
        });
    }
}

exports.p1value = async function (req,res) {
    const id = req.params.id

    let data = await Room.update({
        p1 : req.body.p1
    },{ 
        where : {id}
    });

    res.status(200).json({
        status : "OK",
        data : {
            name : "P1",
            message : "Your Record Has Been Saved!"
        }
    })
}

exports.p2value = async function (req,res) {
    try {
        const id = req.params.id
    
        let data = await Room.update({
            p2 : req.body.p2
        },{ 
            where : {id}
        });
        
        return res.status(200).json({
            status : "OK",
            data : {
                name : "P2",
                message : "Your Record Has Been Saved!"
            }
        })
        
    } catch (err) {
        return res.status(401).json({
            status : "FAIL",
            data : {
                name : "P2",
                message : "FAILED TO INPUT VALUE!"
            }
        })
    }
}

exports.result = async function (req,res) {
    const id = req.params.id;

    const user = await Room.findByPk(id);

    if(user.p1 === "Rock" && user.p2 === "Paper"){
        await user.update({
            result : "P2 Win"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Player 2 Win!"
            }
        })
    }else if(user.p1 === "Rock" && user.p2 === "Scissor"){
        await user.update({
            result : "P1 Win"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Player 1 Win!"
            }
        })
    }else if(user.p1 === "Rock" && user.p2 === "Rock"){
        await user.update({
            result : "Draw"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Game is Draw!"
            }
        })
    }else if(user.p1 === "Paper" && user.p2 === "Rock"){
        await user.update({
            result : "P1 Win"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Player 1 Win!"
            }
        })
    }else if(user.p1 === "Paper" && user.p2 === "Scissor"){
        await user.update({
            result : "P2 Win"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Player 2 Win!"
            }
        })
    }else if(user.p1 === "Paper" && user.p2 === "Paper"){
        await user.update({
            result : "Draw"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "Finish",
                message : "Game is Draw!"
            }
        })
    }else if(user.p1 === "Scissor" && user.p2 === "Rock"){
        await user.update({
            result : "P2 Win"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Player 2 Win!"
            }
        })
    }else if(user.p1 === "Scissor" && user.p2 === "Paper"){
        await user.update({
            result : "P1 Win"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Player 1 Win!"
            }
        })
    }else if(user.p1 === "Scissor" && user.p2 === "Scissor"){
        await user.update({
            result : "Draw"
        })

        return res.status(201).json({
            status : "OK",
            data : {
                name : "FINISH",
                message : "Game is Draw!"
            }
        })
    }

}

exports.gameHistory = async function (req,res) {
    const id = req.params.id;

    const user = await Room.findByPk(id)

    return res.status(200).json({
        status : "OK",
        data : {
            room : user.nameRoom,
            message : user.result       
        }
    })
}