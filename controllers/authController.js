const { User } = require ("../models");
const bcrypt = require ("bcrypt");
const jwt = require ("jsonwebtoken");
const {JWT_SIGNATURE_KEY = "FASYA"} = process.env;

function isPasswordValid(password, encryptedPassword){
    return bcrypt.compareSync(password, encryptedPassword);
}

function createToken(payload){
    return jwt.sign(payload, JWT_SIGNATURE_KEY, {
        expiresIn : '1h'
    });
}

exports.register = async function (req,res){
    try{
        const encryptedPassword = bcrypt.hashSync(req.body.password, 10)

        const user = await User.create({
            username : req.body.username,
            encryptedPassword,
            role : req.body.role
        });

        return res.status(201).json({
            status : "OK",
            data : {
                id : user.id,
                username : user.username,
                createdAt : user.createdAt,
                updatedAt : user.updatedAt
            }
        });
    }catch{err}{
        return res.status(401).json({
            status : "FAIL",
            data : {
                name : "UNPROCESSABLE DATA",
                message : "Cannot Register User"
            }
        });
    }
}

exports.login = async function (req,res) {

    const user = await User.findOne({
        where : {
            username : req.body.username
        }
    });

    if(!user) {
        return res.status(401).json({
            status : "FAIL",
            data : {
                name : "UNAUTHORIZED",
                message : "Username Not Found!"
            }
        });
    }

    if(!isPasswordValid(req.body.password, user.encryptedPassword)){
        return res.status(401).json({
            status : "FAIL",
            data : {
                name : "UNAUTHORIZED",
                message : "Wrong Password!"
            }
        });
    }

    return res.status(201).json({
        status : "OK",
        data : {
            token : createToken({id : user.id, username: user.username, role : user.role})
        }
    })
}

exports.whoami = function (req,res) {
    return res.status(200).json({
        status : "OK",
        data : req.user
    });
}

exports.isAdmin = function (req,res) {
    return res.status(200).json({
        status : "OK",
        data : {
            name: req.user.username,
            message: "You Are Admin!"
        }
    });
}